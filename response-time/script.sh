#!/bin/bash

while true
do
	export ID=$(( ( RANDOM % 1700 )  + 1 ))
	echo $ID
	
	curl -w "@curl-format.txt" -s https://jira01.devtools.intel.com/browse/NAN-$ID -k -o /dev/null
	sleep 5
done
