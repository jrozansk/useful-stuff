#!/bin/bash

while getopts p: opts; do
  case ${opts} in
    p) export http_proxy=${OPTARG}
       export HTTP_PROXY=$http_proxy
       export https_proxy=$http_proxy
       export HTTPS_PROXY=$http_proxy
       ;;
  esac
done

mkdir ~/bin
curl -L -o ~/bin/jq "https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64"
chmod +x ~/bin/jq
sudo yum install bash-completion vi -y
echo "alias completion='source <(kubectl completion bash)'" >> ~/.bashrc
echo "export PATH=$PATH:/home/$USER/bin" >> ~/.bashrc
